package server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;

public class Worker extends Thread {

    private Socket clientSocket;
    private String serverText;
    private BufferedInputStream bis;
    private DataInputStream dis;
    private ArrayList<Worker> fileAttent;
    private PrintWriter output;


    public Worker(Socket clientSocket, String serverText, ArrayList<Worker> fileAttent) {
        this.clientSocket = clientSocket;
        this.serverText   = serverText;
        this.fileAttent= fileAttent;
    }

    public void run() {
        try {
            System.out.println("New Client Connected .......");
            InputStream input  = clientSocket.getInputStream();
            //OutputStream output = clientSocket.getOutputStream();

            output = new PrintWriter(clientSocket.getOutputStream() ,true);

            bis = new BufferedInputStream(input);
            dis = new DataInputStream(bis);





            while (true){
                try {
                    String messageFromClient = dis.readUTF();
                    if (messageFromClient.equals("exit")){
                        break;
                    }
                    System.out.println("CLient [ " + clientSocket.getRemoteSocketAddress() + " ] >> Message: " + messageFromClient);

                    printToAllClients(messageFromClient);
                }catch (IOException e){
//                    System.out.println(e);
                }


            }

            System.out.println("End .......");
            dis.close();
            //output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printToAllClients(String outputString) {


        for(Worker worker : fileAttent) {
            worker.output.println(outputString);
            System.out.println("Sent Message to client: "+ worker.serverText);
        }

    }
}