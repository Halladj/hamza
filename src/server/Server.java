package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection.*;

public class Server extends Thread {
    private int serverPort   = 8080;
    private ServerSocket serverSocket = null;
    private boolean isStopped    = false;
    private Thread runningThread= null;

    private int clientId= 0;

    public Server(int port){
        this.serverPort = port;
    }

    public void run(){
        System.out.println("Server Listening on port: "+this.serverPort) ;

        ArrayList<Worker> fileAttent= new ArrayList<Worker>();
        synchronized(this){
            this.runningThread = Thread.currentThread();
        }
        // OPTAIN AB INSTANCE OF THE ServerSocket Class
        openServerSocket();

        while(! isStopped()){
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if(isStopped()) {
                    System.out.println("......") ;
                    System.out.println("...........") ;
                    System.out.println("Server Stopped.") ;
                    return;
                }
                throw new RuntimeException(
                        "Error accepting client connection", e);
            }
            // SPIN A WORKER TO HANDLE CLIENTS REQUEST
            Worker t= new Worker(
                            clientSocket, "Ronaldo's"+clientId,fileAttent);
            System.out.println(">>>>CLIENT NUMBER  :"+clientId);
            fileAttent.add(clientId++,t);
            t.start();
        }
        System.out.println("Server Stopped.") ;
    }


    private synchronized boolean isStopped() {
        return this.isStopped;
    }



    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port 8080", e);
        }
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }
    public void setRunningThread(Thread runningThread) {
        this.runningThread = runningThread;
    }

    public Thread getRunningThread() {
        return runningThread;
    }


}
