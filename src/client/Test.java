package client;

import java.io.IOException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);

        Client c1 = new Client("localhost", 9000, scan);
        Client c2 = new Client("localhost", 9000, scan);
        Client c3 = new Client("localhost", 9000, scan);
        Client c4 = new Client("localhost", 9000, scan);


        new Thread(c1, "Ronaldo").start();
        new Thread(c2, "Messi").start();
        new Thread(c3, "Ramos").start();
        new Thread(c4, "Zidane").start();
    }
}
