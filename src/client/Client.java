package client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import static java.lang.Thread.currentThread;

public class Client implements Runnable{
    private String serverName;
    private int serverPort;
    private Socket socket = null;
    private BufferedInputStream bis;
    private DataInputStream dis;
    private DataOutputStream dos = null;
    private Scanner scan;

    public Client(String serverName, int serverPort ,Scanner scan) throws IOException {
        this.serverName= serverName;
        this.serverPort = serverPort;
        this.scan= scan;
    }

    @Override
    public void run() {

        System.out.println("Thread name :>>>>>>>"+ currentThread().getName()+"\n");
        try {
                socket = new Socket(serverName, serverPort);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                this.dos = new DataOutputStream(socket.getOutputStream());
                InputStream input  = socket.getInputStream();
                bis = new BufferedInputStream(input);
                dis = new DataInputStream(bis);
            } catch (IOException e) {
                e.printStackTrace();
            }
            synchronized (scan){

                System.out.println(socket.getRemoteSocketAddress());
                System.out.println("Client started on port: "+socket.getLocalPort());
                System.out.println("CLient connected to Server: "+ socket.getRemoteSocketAddress());

                while (true){
                    System.out.println("Message to server : ");
                    String messageToServer = scan.nextLine();

                    try {
                        this.dos.writeUTF(messageToServer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        this.dos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (messageToServer.equals("exit")){
                        break;
                    }

                    try {
                        String messageFromServer = dis.readLine();
                        System.out.println("Broadcast From Server>>>"+messageFromServer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }

            }
            try {
                this.dos.close();
            } catch (IOException e) {
                System.out.println("dos");
//            e.printStackTrace();
            }

            try {
                this.socket.close();
            } catch (IOException e) {
                System.out.println("socket");
//            e.printStackTrace();
            }

    }


}
